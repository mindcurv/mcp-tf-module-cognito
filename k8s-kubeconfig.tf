# generate kubeconfig for convenience
resource "local_file" "kubeconfig" {
  content  = templatefile(
        "${path.module}/k8s-kubeconfig.tftpl",
        {
            eks_cluster_name = data.aws_eks_cluster.cluster.name
            eks_cluster_ca_data = data.aws_eks_cluster.cluster.certificate_authority[0].data
            eks_cluster_api_url = data.aws_eks_cluster.cluster.endpoint
            eks_cluster_arn = data.aws_eks_cluster.cluster.arn
            dex_issuer_url = local.dex_issuer_url
            k8s_client_id = local.k8s_client_id
            k8s_client_secret = local.k8s_client_secret
        }
    )
  filename = "${path.module}/kubeconfig"
}
