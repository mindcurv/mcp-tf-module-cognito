
resource "aws_cognito_user_group" "cluster-admin" {
  name         = "cluster-admin"
  user_pool_id = aws_cognito_user_pool.main.id
  description  = "Kubernetes Cluster Admin (Managed by Terraform)"
}

resource "kubernetes_cluster_role_binding" "cluster-admin" {
  metadata {
    name = "oidc-cluster-admin"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "Group"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
}

resource "aws_cognito_user_group" "grafana-admin" {
  name         = "grafana-admin"
  user_pool_id = aws_cognito_user_pool.main.id
  description  = "Grafana Admin (Managed by Terraform)"
}
