data "aws_eks_cluster" "cluster" {
  name = local.eks_cluster_name
}

resource "aws_eks_identity_provider_config" "main" {
  cluster_name = data.aws_eks_cluster.cluster.name

  oidc {
    client_id                     = local.k8s_client_id
    identity_provider_config_name = "oidc-config-dex"
    issuer_url                    = local.dex_issuer_url
    groups_claim                  = "groups"
    username_claim                = "email"
  }
}
