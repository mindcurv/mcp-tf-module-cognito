# install dex
variable "eks_cluster_dns_suffix" {
  description = "The DNS suffix to attach to ingresses in the cluster"
  type = string
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.default.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.default.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.default.token
  }
}

resource "random_password" "k8s_client_secret" {
  length           = 64
  special          = true
  override_special = "!#-_+:?"
}

# resource "random_password" "argocd_client_secret" {
#   length           = 64
#   special          = true
#   override_special = "!#-_+:?"
# }

# resource "random_password" "grafana_client_secret" {
#   length           = 64
#   special          = true
#   override_special = "!#-_+:?"
# }

locals {
  cognito_issuer_url    = "https://${aws_cognito_user_pool.main.endpoint}"
  cognito_client_id     = aws_cognito_user_pool_client.dex.id
  cognito_client_secret = aws_cognito_user_pool_client.dex.client_secret

  eks_cluster_name      = "${var.mcp_project}-${var.mcp_environment}"

  dex_issuer_url        = "https://dex.${var.eks_cluster_dns_suffix}"
  k8s_client_id         = "k8s-client"
  k8s_client_secret     = random_password.k8s_client_secret.result

#   argocd_client_secret  = random_password.argocd_client_secret.result
  argocd_client_secret  = "argocd-oidc-client-secret"
#   grafana_client_secret = random_password.grafana_client_secret.result
  grafana_client_secret = "grafana-oidc-client-secret"
}

# Client for authenticating kubectl
resource "aws_cognito_user_pool_client" "dex" {
  name = "dex-${local.eks_cluster_name}"
  user_pool_id = aws_cognito_user_pool.main.id

  allowed_oauth_flows = [
    "code"
  ]

  allowed_oauth_scopes = [
    "email",
    "openid",
    "profile",
  ]

  explicit_auth_flows = [
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_ADMIN_USER_PASSWORD_AUTH",
    "ALLOW_USER_PASSWORD_AUTH"
  ] 

  supported_identity_providers = [
    "COGNITO"
  ]

  generate_secret = true

  allowed_oauth_flows_user_pool_client = true

  callback_urls = [
    "https://dex.${var.eks_cluster_dns_suffix}/callback"
  ]
}

resource "helm_release" "dex" {
  namespace = "dex"
  create_namespace = true
  wait      = false

  name = "dex"

  repository = "https://charts.dexidp.io"
  chart      = "dex"
  version    = "0.13.0"

  values = [
    "${templatefile(
        "${path.module}/k8s-helm-dex-values.tftpl",
        {
            cognito_issuer_url      = local.cognito_issuer_url
            cognito_client_id       = local.cognito_client_id
            cognito_client_secret   = local.cognito_client_secret

            dex_issuer_url          = local.dex_issuer_url
            
            k8s_client_id           = local.k8s_client_id
            k8s_client_secret       = local.k8s_client_secret

            argocd_client_secret    = local.argocd_client_secret
            grafana_client_secret   = local.grafana_client_secret

            eks_cluster_dns_suffix  = var.eks_cluster_dns_suffix
            eks_cluster_name        = data.aws_eks_cluster.default.name
        }
    )}"
  ]
}

resource "helm_release" "dex-k8s-authenticator" {
  namespace = "dex-k8s-authenticator"
  create_namespace = true
  wait      = false

  name = "dex-k8s-authenticator"

  repository = "https://charts.sagikazarmark.dev"
  chart      = "dex-k8s-authenticator"
  version    = "0.0.2"

  values = [
    "${templatefile(
        "${path.module}/k8s-helm-dex-k8s-authenticator-values.tftpl",
        {
            dex_issuer_url          = local.dex_issuer_url
            k8s_client_id           = local.k8s_client_id
            k8s_client_secret       = local.k8s_client_secret

            argocd_client_secret    = local.argocd_client_secret
            grafana_client_secret   = local.grafana_client_secret

            eks_cluster_dns_suffix  = var.eks_cluster_dns_suffix
            eks_cluster_endpoint    = data.aws_eks_cluster.default.endpoint
            eks_cluster_ca_data     = base64decode(data.aws_eks_cluster.default.certificate_authority[0].data)
            eks_cluster_name        = data.aws_eks_cluster.default.name
        }
    )}"
  ]
}
