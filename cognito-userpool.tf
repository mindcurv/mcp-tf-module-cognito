# Creates a Cognito userpool for authenticating MCP users.
# E.g. Infrastructure/Management Apps (ArgoCD, Grafana, etc.)
# but also direct kubectl access to Kubernetes via RBAC.

variable "cognito_domain_prefix" {
  description = "The domain prefix that should be used in front in https://oauth-<cognito_domain_prefix>.auth.eu-central-1.amazoncognito.com"
  type    = string
}

variable "mcp_project" {
  description = "The project name for the EKS cluster"
  type = string
}

variable "mcp_environment" {
  description = "The environment for the EKS cluster"
  type = string
}

## Create the User Pool
resource "aws_cognito_user_pool" "main" {
  name = "userpool-${var.cognito_domain_prefix}"

  auto_verified_attributes = [
    "email"
  ]

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "email"
    required                 = true

    string_attribute_constraints {
      min_length = 3
      max_length = 70
    }
  }

  admin_create_user_config {
    allow_admin_create_user_only = true

    invite_message_template {
      email_message = <<EOT
<p>Hi,</p>
<p>you have been invited to access the MCP cluster instance ${data.aws_eks_cluster.cluster.name}.</p>
<hr />
<p>To access the instance you can use these URLs:</p>
<p>
Grafana    - https://monitoring.${var.eks_cluster_dns_suffix}<br />
ArgoCD     - https://argocd.${var.eks_cluster_dns_suffix}<br />
kubeconfig - https://login.${var.eks_cluster_dns_suffix}
</p>
<p>Here are your temporary login credentials:</p>
<p>
Username:           {username}<br />
Temporary Password: {####}
</p>
<hr />
<p>After first login you have to change the password.</p>
<p>Have a wonderful day</p>
EOT
      
      email_subject = "MCP invitation to cluster instance ${data.aws_eks_cluster.cluster.name}"
      
      sms_message = <<EOT
Welcome to MCP cluster instance ${data.aws_eks_cluster.cluster.name}
Username is {username}
Password is {####}
EOT
    }
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }

    recovery_mechanism {
      name     = "verified_phone_number"
      priority = 2
    }
  }

  tags = {
    "Name" = "userpool-${var.cognito_domain_prefix}"
  }
}

## Create the oauth2 Domain

resource "aws_cognito_user_pool_domain" "main" {
  domain = "oauth-${var.cognito_domain_prefix}"
  user_pool_id = aws_cognito_user_pool.main.id
}
